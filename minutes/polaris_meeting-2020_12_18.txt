	   __________________________________________________

		 POLARIS MEETING: LESSONS LEARNED FROM
			     OSCW/BOBCAT-1

			       Hugh Brown
	   __________________________________________________


Table of Contents
_________________

1. [2020-12-18 Fri]  Polaris meeting: Lessons learned from OSCW/Bobcat-1
.. 1. Agenda: lessons learned from OSCW and the Bobcat1 analysis
.. 2. Attending
.. 3. Documentation
.. 4. Data preprocessing for Polaris
.. 5. Threshold adjustment
.. 6. Javascript files
.. 7. Memory
.. 8. Feedback from Kevin
.. 9. TODO Hugh: Create tickets for improving documentation
.. 10. TODO Hugh: File issue to address hosting of javascript files for viz
.. 11. TODO Red: organize discussion/seminar for additional options for polaris learn
.. 12. TODO Kevin: try to get feedback from science members of Bobcat1 about analysis


1 [2020-12-18 Fri]  Polaris meeting: Lessons learned from OSCW/Bobcat-1
=======================================================================

1.1 Agenda: lessons learned from OSCW and the Bobcat1 analysis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


1.2 Attending
~~~~~~~~~~~~~

  - Jan-Peter
  - Adithya
  - Kevin
  - Hugh
  - Red


1.3 Documentation
~~~~~~~~~~~~~~~~~

  - Currently, our documentation is spread between the README and the
    wiki.  We don't cover a lot of use cases
  - Adithya:
    - I suggest we look at using readthedocs for documentation
      generation.  I had a branch at one point working on using
      readthedocs -- can try to dig that up.
    - Should we have a stable API that can be used?  Stable API & CLI
      might be complicated to work around.  Are we targeting developers
      or individual users?
      - Jan-Peter: I'm unsure of diff betw how we use Polaris now &
        using as API.
  - Jan-Peter:
    - When I wanted to analyze Opssat, a walkthrough would have been
      super helpful.
    - I want to update the code diagram over the holidays; began it to
      understand the workflow a bit better, and have found that UML
      might be a good
  - Kevin:
    - Biggest source of help for me has been the chat. [Knew about the
      README, did not know about the wiki.]
    - Readthedocs page is a great idea.
  - Red:
    - docs.polarisml.space would be the obvious place to host this.  We
      have the doc folder, and idea was to have automated documentation
      there -- extract from code.  Pointer to the docs from the README
      would be ideal.
    - Corporate world would love use cases; we should keep an eye on
      this and document them as we come across them.
  - Overall: we seem to have converged on external hosted (ie,
    docs.polarisml.space) rather than wiki or docs folder.  Hugh will
    file issue(s).


1.4 Data preprocessing for Polaris
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - There was a lot of work we had to do on Bobcat1 data before it was
    ready for Polaris to analyze.
  - Red: Kevin's format was one we hadn't come across
    - Kevin: Standard format w/in our dept -- not an external standard.
      - Red: We need to clarify what Polaris can take as input, have as
        documentation -- Bobcat1 as use case
  - Adithya: prob overkill to put much more data preprocessing inside
    polaris; may be some slight improvements for workflow.
    Documentating how to merge spaceweather would be good.
  - Kevin: q re: Polaris -- you run fetch & get one DF, correct?
    (Answer: yes, correct.) Before this, my understanding was we could
    give it multiple Dfs w/timestamps, and Polaris could figure out how
    to merge things
  - JP: That's a good idea/starting point, to have these questions --
    have this in FAQ, have set of answers for this.  Having outside
    users ask these questions is excellent.
  - JP: I'd like a GUI -- still looking for something like this, esp for
    beginners.  Might help in this case.
    - Kevin: pet peeve: I love idea of GUI, but headless servers will
      probably be used for analysis.  Would hate to have features in GUI
      at expense of features in CLI.
    - Red: +1 for GUI hesitation.  Web back end might be good, and would
      make clear that the two are separate.
    - JP will continue investigating this
  - Overall: document what formats Polaris will accept.


1.5 Threshold adjustment
~~~~~~~~~~~~~~~~~~~~~~~~

  - This had to be adjusted by hand in Polaris source code during our
    analysis.
  - Document this as something you might need to adjust during analysis
    of a satellite
  - Now that
    <https://gitlab.com/librespacefoundation/polaris/polaris/-/merge_requests/144>
    is merged (thanks, @deckbsd!), we can do this in a config file
  - Overall: document this.


1.6 Javascript files
~~~~~~~~~~~~~~~~~~~~

  - These had to be fetched each time we ran `viz`.  They should be kept
    in the tree or fetched from CDN (which would be faster, but might
    mean upgrading the version of the library we use)
    - Kevin: +1.  This is a pain point for me
  - Hugh will file issue for this.


1.7 Memory
~~~~~~~~~~

  - Lack of memory for loading the whole Bobcat1 dataset was a huge
    problem.  We're lucky we had Kevin's server with 48 GB of ram, or we
    would have been in serious trouble.
  - Red: This is a limitation of XGBoost; it will load the whole
    dataframe so as to work on everything at same time.  Dask's
    approach, by contrast, is to load bit-by-bit -- much better for
    large datasets.  As it stands, we have to change our processing
    pipeline, or use something like Dask, or something else
    multiprocessor-friendly.
    - Adithya:
      - Dask is integrated into XGBoost, so we can explore this.
      - However, when I was playing with Dask last Feb/Mar, was getting
        *huge* perf hit when using Dask on smaller DS -- 20mins more
        with 200mb datasets; unsure if my mistake, or prob w/Dask.
        Needs more investigation.
        - Red: this is the normal overhead of dask data prep.  Maybe
          Dsak has improved here?  If not, this is for us to manage --
          for example, we could set a threshold for dataset size, and
          use Dask if it's larger than that.
  - Hugh: do we need to think about other libraries/approaches/etc?
    - Red: Yes, learn does just this one thing right now:
      - We picked dependency graph for learn to start with
      - xgboost best for this but has its limits w/memory
      - we can pick things other than dependency graphs to generate, or
        can pick libraries that deal better with large data sets (which
        may have performance tradeoffs)
  - Adithya: chunking data may be another approach
  - JP: what else do we have in mind for learn besides dependency graph?
    - Red:
      - Anomaly detection/autoencoder work from Adithya in the summer
        - Also allows us to navigate in time
      - or graph per behaviour
      - Prediction
  - JP: Suggest we have a discussion around learning strategies
    - Hugh: +1
    - TODO: Red: plan a discussion/seminar for this.
  - Red: We may want to talk to the LSF about servers to do this kind of
    analysis -- high memory, good GPU, or maybe both.


1.8 Feedback from Kevin
~~~~~~~~~~~~~~~~~~~~~~~

  - Question for Kevin: you're new to Polaris, and your POV is valuable.
    What is Polaris missing?
    - Kevin: *weakest point of polaris is visualization*. Trying to pan,
      orbit, isolate a link is very hard.  Cumbersome to figure out &
      zoom in.
      - JP: Can you send a screenshot of what you mean?
        - Kevin: maybe it's my browser, but glitches when I use it.
          - Might be worth trying Chrome/Chromium here
      - Red: Understand. Want to use the heads-up display for more --
        display numbers, etc
        - Kevin: that would help.  So would having links to next items.
      - Red: One thing we've thought about is showing the
        quickest/shortest path between two nodes; this could be used,
        for example, in the case where you want to affect/change a
        particular item, and you need to figure out the best way to do
        this.
        - Kevin: this sounds really cool!
    - Red: Polaris viz workshop would be good
    - Note from Hugh: I'm not sure we came to a conclusion on what to do
      here.  We should continue discussing this in the chat room, and
      look for ways to sit down with Kevin to discuss this further.
  - What did Polaris do well?
    - Kevin: This was the first time we put science data into Polaris,
      so expecting hassles.  But this has been really neat.
      - Hugh: Would love to find out what this means to you -- what has
        the analysis shown you?  What does the graph mean in erms of
        science?
        - Kevin: sure, I can get feedback from teammates on what the
          graph means to you.  If we have a meeting next week, I can get
          this; otherwise, will try to pin down advisor.


1.9 TODO Hugh: Create tickets for improving documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Stuff we want in there
    - more use cases (Bobcat1, for example)
    - What formats Polaris can accept as input
    - How to preprocess data
    - What settings you should consider playing with
  - Migrating from wiki
  - Picking a tool to generate/manage docs (readthedocs a strong
    contender)
  - where to host (ie, docs.polarisml.space)


1.10 TODO Hugh: File issue to address hosting of javascript files for viz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


1.11 TODO Red: organize discussion/seminar for additional options for polaris learn
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - What other analyses we can do
  - Strategies for dealing with large data sets


1.12 TODO Kevin: try to get feedback from science members of Bobcat1 about analysis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
