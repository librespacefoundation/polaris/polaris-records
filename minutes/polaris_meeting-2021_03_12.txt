	  ____________________________________________________

		    POLARIS MEETING - MARCH 13, 2021

	   Hugh Brown <aardvark@saintaardvarkthecarpeted.com>
	  ____________________________________________________


Table of Contents
_________________

1. [2021-03-12 Fri]  - Satellite operators, hackathon
.. 1. Attending
.. 2. How do we get in front of satellite operators?
.. 3. Hackathon for Betsi visualization
.. 4. Generated graphs for satellites?
.. 5. Action items
..... 1. TODO Hugh to talk to Acinonyx / @cshields about servers for Polaris analysis
..... 2. TODO Hugh: to get current doc to Nikoletta/Elkos & get feedback
..... 3. TODO Hugh: Talk to oldbug, phasewrap about current dos
..... 4. TODO Discuss hackathon next Friday
..... 5. TODO Nikoletta: to start outline & get back to us


1 [2021-03-12 Fri]  - Satellite operators, hackathon
====================================================

1.1 Attending
~~~~~~~~~~~~~

  - Nikoletta, Elkos, Hugh, Xabi, Adithya


1.2 How do we get in front of satellite operators?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Nikoletta: We have a community -- can post in channels, blog about
    this, etc -- but Elkos has more info.
  - Elkos:
    - One point: make it easy for sat ops can do stuff on their own.
      Example: Dashboard.satnogs.org: people can work w/satellite
      operators to build these dashboards.  Sometimes we see satellites
      no longer being operated by original team: maybe original team has
      moved on to another project, etc. So someone who's familiar
      w/project can talk to Oldbug -- he's one of the most active people
      in dashboards -- and will be able to proceed.
    - Does Polaris current tooling allow for this?  Can a satellite
      operator create this kind of visualization?  This is something I'd
      like to understand: we need to put out documentation where
      satellite operators can find & use it.  I couldn't find this
      documentation easily -- if I can't find it, others will have
      problems as well.
      - Example: we tell people to go to dashboards.satnogs.org & play
        around - could they do the same with Polaris?
    - Another point: both Oldbug & Phasewrap are talking with operators
      a lot already.  If we can point Oldbug & Phasewrap at our
      documentation, we can get this in front of operators.
    - Use SatNOGS as an anchor: if a team has enough active members to
      create a dashboard, chances are you can find someone who's
      interested in visualization with Polaris.  If team not responsive
      to SatNOGS, may not be operating anymore -- maybe
      important/central team members have moved on.
    - We should focus on having easy way to see the possibilities: "This
      could be your own satellite being displayed here!  Talk to your
      Polaris rep today to get started."
    - One thing we were planning for social media in coming weeks:
      Kevin's talk about Polaris on OSCW.  Not just that it's pretty --
      it's helpful, and you (the satellite operator) can do it.  If we
      pair that with nice documentation, we can say: "Here's what Kevin
      did; here's the Polaris community where you can get help and
      suggest improvements."  Nikoletta can suggest improvements in
      content, or things we're missing.
    - Have step-by-step instructions; have chat room where people can
      get help.  Once we have these things in place, we'll be much
      further ahead -- and this can be a great example of using open
      data.
  - Xabi: Using OSCW is a good idea.  We can write up a tutorial and
    post both together -- would that work?
    - Elkos: Yep.  Maybe long format blgo post as well
    - Nikoletta: As with GSOC article, good to have info all in one
      place, one link: we post that & everyone can get on same page.
      Having one place to send people very important.
  - Elkos: adding mention on SatNOGS wiki is definitely a possibility --
    extra box; "Now that you've finished integrating your mission
    w/SatNOGS, look how to make more use of your data here!"
    - We tell people to read the guide -- we can add a section about
      Polaris, pointing to P. doc, see the visualization they can get
      with this.
  - Hugh: N & E: can you look at our doc now & see how it is? And would
    we write up a blog post, or something else?
    - Nikoletta: we can collaborate on blog post -- work on outline, N
      can write, we can improve ; balance for many audiences.  Needs to
      be readable, but needs to have interesting info, but not overdone


1.3 Hackathon for Betsi visualization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Xabi: Orig thinking of something small & internal -- but @DeckBSD
    suggested something larger.
  - Elkos: need to focus on what we want to achieve.  First you have to
    educate people about Polaris in order for this to be relevant.
    - If you're doing an internal hackathon, we can support you.
    - Issue is that if you open hackathon on very specific issue, might
      need more time to educate people on the context -- get them
      comfortable with Polaris, Betsi.  You need to spend more time on
      this than maybe on the hackathon itself.  Not a bad thing, but
      something to keep in mind.
    - Could ask people to join us on Jitsi, chat, etc.
    - One other thing: we have a lot of newcomers -- this is good!  But
      we're doing a lot of time getting people up to speed: how Polaris,
      Open Source,git works etc.  (Adithya is doing MASSIVE work here!)
      - Maybe something like this hackathon would be a good way to find
        people who already have a good background.
      - And let people bring their own ideas -- Adithya another good
        example here.  Expose people to challenges and see what they can
        do.
    - Newcomers will be in hackathon, even if internal.  This is good --
      figure out how to make the best of their efforts.
    - May be good way to introduce Polaris to other LSF contributors --
      remind them that what they make may be used by Polaris later.
      "Internal" might include all of LSF...encourage collaboration
      between LSF projects.
  - Nikoletta: Hackathon great idea, whether internal/external.  Even if
    done internally & don't get a lot of publicity out of it *for* the
    event itself, we can write blog post afterward: present as a case
    study about this. What problem is, what we tried, what came out of
    it.  Post-mortem.  These are always very useful in themselves, but
    also bring in a lot of readers as well.
  - Xabi: what comes next?
    - Elkos: picking out platform: Matrix/Jitsi/hybrid of the two?
    - Hugh: probably discussing internally with team -- let's talk about
      this next Friday.


1.4 Generated graphs for satellites?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Elkos: Talk to Acinonyx (Vasilis).  We may be getting a few new
    computers in hackerspace shortly that might be useful -- he'll know
    more.  Corey Shields too.


1.5 Action items
~~~~~~~~~~~~~~~~

1.5.1 TODO Hugh to talk to Acinonyx / @cshields about servers for Polaris analysis
----------------------------------------------------------------------------------


1.5.2 TODO Hugh: to get current doc to Nikoletta/Elkos & get feedback
---------------------------------------------------------------------


1.5.3 TODO Hugh: Talk to oldbug, phasewrap about current dos
------------------------------------------------------------


1.5.4 TODO Discuss hackathon next Friday
----------------------------------------


1.5.5 TODO Nikoletta: to start outline & get back to us
-------------------------------------------------------
